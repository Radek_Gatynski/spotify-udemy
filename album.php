<?php 
include './includes/includedFiles.php';

if(isset($_GET['id'])){
    $albumID = $_GET['id'];

}else{
    header("location: index.php");
}
$album = new Album($conn,$albumID);
//echo $album->getTitle();
//można jeśli zmienimy funkcję w Album.php
//echo $album->getArtist();
//echo $album->getArtist()->getName();
//$artist = new Artist($conn,$album['artist']);
?>
<div class="entityInfo">
    <div class="leftSection">
         <img src="<?php echo $album->getArtworkPath();?>">
    </div>

    <div class="rightSection">
        <h2><?php echo $album->getTitle();?> </h2>
        <p>By: <?php echo $album->getArtist()->getName();?> </p>
        <p><?php echo $album->getNumberOfSongs();?>. Songs</p>
    </div>
</div>
<div class="trackListContainer">
    <ul>
        <?php 
            $songsArray = $album->getSongIDs();
        
            $i = 1;
            foreach($songsArray as $songID){
               $albumSong = new Song($conn,$songID);
               $albumArtist = $albumSong->getArtist();
               echo "<li class='trackListRow'>
                    <div class='trackCount text-center'>
                         <img class='playAlbum' src='./assets/images/icons/play-white.png' onclick='setTrack(\"" . $albumSong->getID() . "\", tempPlayList, true)'>
                         <span class='trackNumber'>$i.</span>
                    </div>
                    <div class='trackInfo'>
                        <span class='trackName'>" . $albumSong->getTitle() . "</span><br />
                        <span class='artistName'>" . $albumArtist->getName() . "</span>
                    </div>
                    <div class='trackOption'>
                    <input type='hidden' class='songID' value='" . $albumSong->getID() ."'>
                        <img src='./assets/images/icons/more.png' alt='more options' class='optionButton' onclick='showOptionMenu(this)'>
                    </div>
                    <div class='trackDuration'>
                        <span class='artistName'>" . $albumSong->getDuration() . "</span>
                    </div>
               </li>";
               $i = $i +1;
            }
        ?>
        <script>
            var tempSongIds = '<?php echo json_encode($songsArray); ?>';
            tempPlayList = JSON.parse(tempSongIds);
        </script>
    </ul>
</div>

<nav class="optionMenu">
            <input type="hidden" class="songID">
            <?php echo Playlist::getPlaylistDropdown($conn,$userLoggedIn->getUsername()); ?>
</nav>
