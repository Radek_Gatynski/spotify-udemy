<?php
include "./includes/includedFiles.php";
if(isset($_GET['id'])){
    $artistID = $_GET['id'];
}else{
    header("location: index.php");
}
$artist = new Artist($conn, $artistID);
?>
<div class="entityInfoArtist">
    <div class="centerSection">
        <div class="artistInfo">
            <h1 class="artistName text-center"><?php echo $artist->getName();?></h1>
            <div class="headerButton text-center">
                <button class="btn btn-success" onclick="playFirstSong()">PLAY</button>
            </div>
        </div>
    </div>
</div>
<div class="trackListContainer artistContainer">
    <h2 class="text-center">Artist songs</h2>
    <ul>
        <?php 
            $songsArray = $artist->getSongsIDs();
        
            $i = 1;
            foreach($songsArray as $songID){
                if($i>5){
                    break;
                }
               $albumSong = new Song($conn,$songID);
               $albumArtist = $albumSong->getArtist();
               echo "<li class='trackListRow'>
                    <div class='trackCount text-center'>
                         <img class='playAlbum' src='./assets/images/icons/play-white.png' onclick='setTrack(\"" . $albumSong->getID() . "\", tempPlayList, true)'>
                         <span class='trackNumber'>$i.</span>
                    </div>
                    <div class='trackInfo'>
                        <span class='trackName'>" . $albumSong->getTitle() . "</span><br />
                        <span class='artistName'>" . $albumArtist->getName() . "</span>
                    </div>
                    <div class='trackOption'>
                        <img src='./assets/images/icons/more.png' alt='more options' class='optionButton'>
                    </div>
                    <div class='trackDuration'>
                        <span class='artistName'>" . $albumSong->getDuration() . "</span>
                    </div>
               </li>";
               $i = $i +1;
            }
        ?>
        <script>
            var tempSongIds = '<?php echo json_encode($songsArray); ?>';
            tempPlayList = JSON.parse(tempSongIds);
        </script>
    </ul>
</div>
<div class="gridViewContainer">
    <h2 class="text-center">Artist albums</h2>
            <?php
                $albumSQL = mysqli_query($conn, "Select * from albums where artist='$artistID'");
                while($row = mysqli_fetch_array($albumSQL)){
                    echo 
                    "<div class='gridViewItem'>  
                        <a href='album.php?id=" . $row['id'] . "'>
                            <img src='" . $row['artworkPath'] . "'>
                                <div class='gridViewInfo text-center'>"
                                . $row['title'] .
                                "</div>
                        </a>
                    </div>";
                }
            ?>
</div>