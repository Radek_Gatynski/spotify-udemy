var currentPlayList =  [];
var shufflePlayList = [];
var tempPlayList = [];
var audioElement;
var mouseDown = false;
var currentIndex = 0;
var repeat = false;
var shuffle = false;
var userLoggedIn;
var timer;

$(document).click(function(click){
    var target = $(click.target);
    if(!target.hasClass('item') && !target.hasClass("optionButton")){
        hiddeOptionMenu();
    }
});


$(window).scroll(function(){
    hiddeOptionMenu();
});

$(document).on("change", "select.playlist", function(){
    var select = $(this);
    var playlistID = select.val();
    var songID = select.prev(".songID").val();
    $.post("./includes/handles/ajax/addToPlayList.php",{playlistID:playlistID, songID:songID})
    .done(function(error){
        if(error!=null){
            alert(error);
            return
        }
        hiddeOptionMenu();
        select.val("");
    });
});

function removeFromPlaylist(button, playlistID){
    var songID = $(button).prevAll(".songID").val();
    $.post("./includes/handles/ajax/removeFromPlayList.php",{playlistID:playlistID, songID:songID})
    .done(function(error){
        if(error!=null){
            alert(error);
            return
        }
        openPage("playlist.php?id="+playlistID);
    });
}
function logout(){
    $.post("./includes/handles/ajax/logoutUser.php", function(){
        location.reload();
    });
}

function openPage(url){
    if(timer != null){
        clearTimeout(timer);
    }
    if(url.indexOf("?") == -1){
        url = url + "?";
    }

    var encodedUrl = encodeURI(url + "&userLoggedIn=" + userLoggedIn);
    $("#mainContent").load(encodedUrl);
    $("body").scrollTop(0);
    history.pushState(null,null,url);
}


function createPlaylist(){
 //   promp('Please enter the name of your playlist');
    var popup = prompt('Please enter the name of your playlist');
    if(popup !=null){
        $.post("./includes/handles/ajax/createPlaylist.php",{name:popup, username: userLoggedIn})
        .done(function(error){
            if(error!=null){
                alert(error);
                return;
            }
            openPage('yourMusic.php');
        });
    }
}

function formatTime(seconds){
    var time = Math.round(seconds);
    var minutes = Math.floor(time/60);
    var seconds = time - minutes * 60;
    var extraZero;
    if(seconds<10){
        extraZero = "0";
    }else{
        extraZero = ""
    }
    return minutes + ":" + extraZero + seconds;
}

function updateTimeProgressBar(audio){
    $(".progressTime.current").text(formatTime(audio.currentTime));
    $(".progressTime.remaining").text(formatTime(audio.duration-audio.currentTime));
    var progress = audio.currentTime / audio.duration * 100;
    $(".progressInfo").css("width",progress+"%")
}
function updateVolumeProgressBar(audio){
    var volume = audio.volume * 100;
    $(".progressVolume .progressInfoVolume").css("width", volume+"%");
}


function playFirstSong(){
    setTrack(tempPlayList[0],tempPlayList,true);
}


function deletePlaylist(id){
    var prompt = confirm("Are you sure?");
    if(prompt == true){
        $.post("./includes/handles/ajax/deletePlaylist.php",{playlistID:id})
        .done(function(error){
            if(error!=null){
                alert(error);
                return
            }
            openPage('index.php');
        })
    }
}

function showOptionMenu(button){
    var songID = $(button).prevAll(".songID").val();

    var menu = $(".optionMenu");
    var menuWidth = menu.width();
    
    menu.find(".songID").val(songID);
    
    var scrollTop = $(window).scrollTop(); 
    var elementOffset = $(button).offset().top;
    var top = elementOffset-scrollTop;

    var left = $(button).position().left;
    menu.css({"top":top+"px", "left":left-menuWidth +"px","display":"inline"});
}

function hiddeOptionMenu(){
    var menu = $(".optionMenu");
    if(menu.css("display") != "none"){
        menu.css("display", "none");
    }
}
function Audio(){
    this.currentlePlaying;
    this.audio = document.createElement('audio');
    this.audio.addEventListener("ended", function(){
        nextSong();
    });


    this.audio.addEventListener("canplay", function(){
        $(".progressTime.remaining").text(formatTime(this.duration));
        
    });

    this.audio.addEventListener("timeupdate", function(){
        if(this.duration){
            updateTimeProgressBar(this);
        }
    });

    this.audio.addEventListener("volumechange", function(){
        updateVolumeProgressBar(this);
    })

    this.setTrack = function(track){
        this.currentlePlaying = track;
        this.audio.src = track.path;
    }

    this.play = function(){
        this.audio.play();
    }
    
    this.pause = function(){
        this.audio.pause();
    }

    this.setTime = function(seconds){
        this.audio.currentTime = seconds;
    }
}

