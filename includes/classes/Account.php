<?php
class Account{
    private $errors;
    private $conn;
    public function __construct($conn){
        $this->errors = array();
        $this->conn = $conn;
    }

    public function login($login, $password){
        $pass = md5($password);
        $query = mysqli_query($this->conn, "select * from users where login='$login' and password='$pass'");
        if(mysqli_num_rows($query) ==1 ){
            return true;
        }
        else{
            array_push($this->errors, Constants::$loginFail);
            return false; 
        }
    }

    public function register($uN, $uE, $pass, $passConf){
        $this->validateLogin($uN);
        $this->validateEmail($uE);
        $this->validatePassword($pass, $passConf);
        if(empty($this->errors)){
            return $this->insertUserDetail($uN,$uE,$pass);
        }
        else{
            return false;
        }
    }

    private function insertUserDetail($uN,$uE,$pass){
        $encryptedPass = md5($pass);
        $profilePic = "../../assets/images/profilePic/profileDefaultPix.png";
        $results = mysqli_query($this->conn,"Insert into users values ('','$uN','$uE','$encryptedPass','','$profilePic')");
        if($results){
            echo "Poprawnie wrzucono";
            return $results;
        }
        else{
            array_push($this->errors, Constants::$dbInsertUserError);
        }
        
    }
    public function getError($error){
        if(!in_array($error, $this->errors)){
            $error = "";
        }
        return "<small class='text-success'>$error</small>";
    }
    private function validateLogin($uN){
        if(strlen($uN)<5 || strlen($uN)>30){
            array_push($this->errors, Constants::$userName);
            return;
        }
        $checkIfExist = mysqli_query($this->conn, "Select login from users where login='$uN'");
        if(mysqli_num_rows($checkIfExist) == 1){
            array_push($this->errors, Constants::$userNameExists);
        }
        
    }
    private function validateEmail($uE){
        if(!filter_var($uE, FILTER_VALIDATE_EMAIL)){
            array_push($this->errors, Constants::$userEmail);
            return;
        }
        $checkEmailExist = mysqli_query($this->conn, "Select email from users where email='$uE' Limit 1");
        if(mysqli_num_rows($checkEmailExist) == 1){
            array_push($this->errors, Constants::$userNameExists);
        }
    }

    private function validatePassword($pass, $passConf){
        if($pass != $passConf){         
            array_push($this->errors, Constants::$passwordsDoNotMatch);
            return;
        }
        if(preg_match('/[^A-Za-z0-9]/', $pass)){
           array_push($this->errors, Constants::$passwordRegrex);
            return;
        }
        if(strlen($pass)>30 || strlen($pass)<5){
            array_push($this->errors, Constants::$passwordLength);
        }
    }
}

?>