<?php 
class Album{
    private $conn;
    private $id;
    private $title;
    private $artistID;
    private $genre;
    private $artworkPath;
    public function __construct($conn,$id){
        $this->conn=$conn;
        $this->id=$id;
        $albumsSQL = mysqli_query($this->conn,"Select * from albums where id='$this->id'");
        $album = mysqli_fetch_array($albumsSQL);
        $this->title = $album['title'];
        $this->artistID = $album['artist'];
        $this->genre = $album['genre'];
        $this->artworkPath = $album['artworkPath'];
    }

    public function getTitle(){
        return $this->title;
    }
    public function getArtworkPath(){
        return $this->artworkPath;
    }
    public function getArtist(){
       // $artist = new Artist($this->conn, $this->artistID);
       // return $artist->getName();
       return new Artist($this->conn, $this->artistID);
    }
    public function getGenre(){
        return $this->genre;
    }
    public function getNumberOfSongs(){
        $sql = mysqli_query($this->conn,"Select id from songs where album='$this->id'");
        return mysqli_num_rows($sql);
    }
    public function getSongIDs(){
       $sql = mysqli_query($this->conn,"Select id from songs where album='$this->id' Order by albumOrder ASC");
       $array = array();
       while($row = mysqli_fetch_array($sql)){
           array_push($array, $row['id']);
       } 
       return $array;
    }
}
?>