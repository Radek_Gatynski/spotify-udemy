<?php
class Artist{
    private $conn;
    private $id;
    public function __construct($conn, $id){
        $this->conn = $conn;
        $this->id = $id;
    }
    public function getName(){
        $artistSQL = mysqli_query($this->conn, "select name from artists where id='$this->id'");
        $artist = mysqli_fetch_array($artistSQL);
        return $artist['name'];
    }
    public function getSongsIDs(){
        $sql = mysqli_query($this->conn,"Select id from songs where artist='$this->id' Order by albumOrder ASC");
        $array = array();
        while($row = mysqli_fetch_array($sql)){
            array_push($array, $row['id']);
        } 
        return $array;
    }
    public function getID(){
        return $this->id;
    }
}
?>