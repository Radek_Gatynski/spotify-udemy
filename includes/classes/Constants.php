<?php
class Constants{
    public static $passwordsDoNotMatch = "Password and repeat password are not this same";
    public static $userName = "Username must be between 5 and 30 characters";
    public static $userEmail = "User email is wrong";
    public static $passwordRegrex = "Password can only contain letters and numbers";
    public static $passwordLength = "The password must be between 5 and 30 characters";
    public static $userNameExists = "The user name with this name exists";
    public static $emailExists = "This email address already exists in database";

    public static $dbInsertUserError = "Error when inserted new user do database";
    
    //LOGINS
    public static $loginFail = "Login system fails";

}
?>