<?php
class Playlist{
    private $conn;
    private $id;
    private $name;
    private $owner;

    public function __construct($conn, $data){
        if(!is_array($data)){
            $sql = mysqli_query($conn, "select * from playlist where id='$data'");
            $data = mysqli_fetch_array($sql);
        }
        $this->conn = $conn;
        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->owner = $data['owner'];
    }
    public function getName(){
        return $this->name;
    } 
    public function getOwner(){
        return $this->owner;
    }
    public function getID(){
        return $this->id;
    }
    public function getNumerOfSongs(){
        $sql = mysqli_query($this->conn, "Select songID from playlistsongs where playlistID='$this->id'");
        return mysqli_num_rows($sql);
    }

    public function getSongsIdFromPlaylist(){
        $sql = mysqli_query($this->conn, "Select songID from playlistsongs where playlistID='$this->id'");
        $array = array();
        while($row = mysqli_fetch_array($sql)){
            array_push($array, $row['songID']);
        } 
        //$array = mysqli_fetch_array($sql);
        return $array;
    }


    public function getArtist(){
        // $artist = new Artist($this->conn, $this->artistID);
        // return $artist->getName();
        return new Artist($this->conn, $this->artistID);
     }
    public function getSongIDs(){
        $sql = mysqli_query($this->conn,"Select id from songs where album='$this->id' Order by albumOrder ASC");
        $array = array();
        while($row = mysqli_fetch_array($sql)){
            array_push($array, $row['id']);
        } 
        return $array;
     }

     public static function getPlaylistDropdown($conn, $username){ 
        $dropdown =  "<select class='item playlist'><option value=''>Add to playlist</option>";
        $sql = mysqli_query($conn,"SELECT * FROM `playlist` WHERE `owner` = '$username'");

        while($row = mysqli_fetch_array($sql)){
            $id = $row['id'];
            $name = $row['name'];
            $dropdown = $dropdown . "<option value='$id'>'$name'</option>";
        }
        $dropdown = $dropdown . "</select>";
      return $dropdown;
     } 
}
?>