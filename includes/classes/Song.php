<?php
class Song{
    private $conn;
    private $id;
    private $mysqliData;
    private $title;
    private $artistID;
    private $albumID;
    private $genre;
    private $duration;
    private $path;

    public function __construct($conn,$id){
        $this->conn=$conn;
        $this->id=$id;
        $sql = mysqli_query($this->conn,"Select * from songs where id='$this->id'");
        $this->mysqliData = mysqli_fetch_array($sql);
        $this->title = $this->mysqliData['title'];
        $this->artistID = $this->mysqliData['artist'];
        $this->albumID = $this->mysqliData['album'];
        $this->genre = $this->mysqliData['genre'];
        $this->duration = $this->mysqliData['duration'];
        $this->path = $this->mysqliData['path'];
    }
    public function getID(){
        return $this->id;
    }
    public function getTitle(){
        return $this->title;
    }
    public function getArtist(){
        return new Artist($this->conn,$this->artistID);
    }
    public function getAlbum(){
        return new Album($this->conn,$this->albumID);
    }
    public function getDuration(){
        return $this->duration;
    }
    public function getPath(){
        return $this->path;
    }
    public function getGenre(){
        return $this->genre;
    }
    public function getMysqliData(){
        return $this->mysqliData;
    }

}
?>