<?php
class User{
    private $conn;
    private $username;
    public function __construct($conn, $username){
        $this->conn = $conn;
        $this->username = $username;
    }
    public function getUsername(){
        return $this->username;
    } 
    public function getFirstAndLastName(){
        $sql = mysqli_query($this->conn,"Select concat(login, ' (', email, ')') as 'name' from users");
        $row = mysqli_fetch_array($sql);
        return $row['name'];
    }
}
?>