<?php 
function trimInputFields($inputText){
    $inputText = strip_tags($inputText);
    $inputText = str_replace(" ", "", $inputText);
    return $inputText;
}

function trimPassword($inputText){
    $inputText = strip_tags($inputText);
    return $inputText;
}

function trimString($inputText){
    $inputText = strip_tags($inputText);
    $inputText = str_replace(" ","", $inputText);
    $inputText = ucfirst(strtolower($inputText));
    return $inputText;
}

if(isset($_POST['registerUser'])){
    $registerData = array("login" => '', "email"=> '', "password" => '', "passwordConfirm" => '');
    $registerData["login"] = trimInputFields($_POST['userLogin']);
    $registerData["email"] = trimString($_POST['userEmail']);
    $registerData["password"] = trimInputFields($_POST['userPassword']);
    $registerData["passwordConfirm"] = trimInputFields($_POST['userPasswordRepeat']);
    $wasSuccess = $account->register($registerData["login"], $registerData["email"], $registerData["password"], $registerData["passwordConfirm"]);

    if($wasSuccess == true){
        $_SESSION["userLoggedIn"] = $registerData["login"];
        header("Location: index.php");
    }
}
?>