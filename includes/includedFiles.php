<?php 

if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
    include_once "./includes/dbConnect.php";
    include_once './includes/classes/User.php';
    include_once './includes/classes/Artist.php';
    include_once './includes/classes/Album.php';
    include_once './includes/classes/Song.php';
    include_once './includes/classes/Playlist.php';
    if(isset($_GET['userLoggedIn'])){
        $userLoggedIn = new User($conn, $_GET['userLoggedIn']);
    }
    else{
        echo "Username variable was not passed into page. Check openPage js function";
        exit();
    }
}
else{
    include_once './templates/header.php';
    include_once './templates/footer.php';
    $url = $_SERVER['REQUEST_URI'];
    echo "<script>openPage('$url')</script>";
    exit();
}

?>