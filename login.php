<?php
include "./templates/headerPureHTML.php";
include "./includes/classes/Account.php";
include "./includes/classes/Constants.php";
include "./includes/dbConnect.php";
$account = new Account($conn);
include "./includes/handles/login-handles.php";

function displaySetValue($inputName){
    if(isset($_POST[$inputName])){
        echo $_POST[$inputName];
    }
}

?>
<div class="register-and-login">
        <form action="login.php" method="POST">
            <header>
                <h2 class="text-center">Login</h2>
            </header>
            <div class="form-group">
                <label for="userName">Name</label>
                <input  class="form-control type="text" name="userName" id="userName" placeholder="Enter your name" required value="<?php displaySetValue('userName'); ?>">
            </div>
            <div class="form-group">
                <label for="userPassword">Password</label>
                <input  class="form-control" type="password" name="userPassword" id="userPassword" placeholder="Enter yout password" required>         
            </div>
            <input type="submit" name="login" value="Login" class="btn btn-primary">
            <?php echo $account->getError(Constants::$loginFail); ?>            
        <a href="register.php">Don't have account. Please register!</a>
        </form>
</div>

<?php include "./templates/footerPureHTML.php"; ?>