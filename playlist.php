<?php 
include './includes/includedFiles.php';

if(isset($_GET['id'])){
    $playlistID = $_GET['id'];

}else{
    header("Location: index.php");
}
$playlist = new Playlist($conn,$playlistID);
$owner = new User($conn,$playlist->getOwner());
?>
<div class="entityInfo">
    <div class="leftSection">
         <img src="./assets/images/icons/playlist.png">
    </div>

    <div class="rightSection">
        <h2><?php echo $playlist->getName();?></h2>
        <p>By: <?php echo $playlist->getOwner();?> </p>
        <p><?php echo $playlist->getNumerOfSongs();?>. Songs</p>
        <button class='btn btn-danger' onclick="deletePlaylist('<?php echo $playlistID;?>')">Delete Playlist</button>
    </div>
</div>
<div class="trackListContainer">
    <ul>
        <?php 
            $playListSongsArray = $playlist->getSongsIdFromPlaylist();
            $i = 1;
            foreach($playListSongsArray as $songID){
               $playlistSong = new Song($conn,$songID);
               $songArtist = $playlistSong->getArtist();
               echo "<li class='trackListRow'>
                    <div class='trackCount text-center'>
                         <img class='playAlbum' src='./assets/images/icons/play-white.png' onclick='setTrack(\"" . $playlistSong->getID() . "\", tempPlayList, true)'>
                         <span class='trackNumber'>$i.</span>
                    </div>
                    <div class='trackInfo'>
                        <span class='trackName'>" . $playlistSong->getTitle() . "</span><br />
                        <span class='artistName'>" . $songArtist->getName() . "</span>
                    </div>
                    <div class='trackOption'>
                    <img src='./assets/images/icons/more.png' alt='more options' class='optionButton' onclick='showOptionMenu(this)'>
                    </div>
                    <div class='trackDuration'>
                        <span class='artistName'>" . $playlistSong->getDuration() . "</span>
                    </div>
               </li>";
               $i = $i +1;
            }
        ?>
        <script>
            var tempSongIds = '<?php echo json_encode($playListSongsArray); ?>';
            tempPlayList = JSON.parse(tempSongIds);
        </script>
    </ul>
</div>
<nav class="optionMenu">
            <input type="hidden" class="songID">
            <?php echo Playlist::getPlaylistDropdown($conn,$userLoggedIn->getUsername()); ?>
            <div class="item" onclick="removeFromPlaylist(this,'<?php echo $playlistID;?>')">Remove from playlist</div>
</nav>
