<?php
include "./templates/headerPureHTML.php";
include "./includes/classes/Account.php";
include "./includes/classes/Constants.php";
include "./includes/dbConnect.php";
$account = new Account($conn);
include "./includes/handles/register-handles.php";

function displaySetValue($inputName){
    if(isset($_POST[$inputName])){
        echo $_POST[$inputName];
    }
}

?>
<div class="register-and-login">
    <form action="register.php" method="POST">
        <header>
            <h2 class="text-center">Registration</h2>
        </header>
        <?php echo $account->getError(Constants::$dbInsertUserError); ?>
        <div class="form-group">
            <label for="userLogin">Login</label>
            <input  class="form-control type="text" name="userLogin" id="userLogin" placeholder="Enter your login" required value="<?php displaySetValue("userLogin"); ?>">
            <?php echo $account->getError(Constants::$userName); ?>
            <?php echo $account->getError(Constants::$userNameExists); ?>
        </div>
        <div class="form-group">
            <label for="userEmail">Email</label>
            <input  class="form-control type="email" name="userEmail" id="userEmail" placeholder="Enter your email address" required value="<?php displaySetValue("userEmail"); ?>">
            <?php echo $account->getError(Constants::$userEmail); ?>
            <?php echo $account->getError(Constants::$userNameExists); ?>
        </div>
        <div class="form-group">
            <label for="userPassword">Password</label>
            <input  class="form-control" type="password" name="userPassword" id="userPassword" placeholder="Enter your password" required value="<?php displaySetValue("userPassword"); ?>">
            <?php echo $account->getError(Constants::$passwordRegrex); ?>
            <?php echo $account->getError(Constants::$passwordLength);?>
        </div>
        <div class="form-group">
            <label for="userPasswordRepeat">Password Repeat</label>
            <input  class="form-control" type="password" name="userPasswordRepeat" id="userPasswordRepeat" placeholder="Repeat your password" required value="<?php displaySetValue("userPasswordRepeat") ?>">
            <?php echo $account->getError(Constants::$passwordsDoNotMatch);?>
        </div>
        <div class="form-group">
            <input type="submit" value="Register" name="registerUser" class="btn">
        </div>
        <a href="login.php">Already have account?</a>
    </form>   
</div>
<?php include "./templates/footerPureHTML.php"; ?>
