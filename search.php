<?php
include "./includes/includedFiles.php";
if(isset($_GET['target'])){
    $target = urldecode($_GET['target']);
}else{
    $target = "";
}
?>



<div class="searchContainer">
    <h4>Search for artist, album or song</h4>
    <label for="search"></label>
    <input type="text" class="form-control searchItem" id="search" aria-describedby="searchHelp" placeholder="Enter text" value="<?php echo $target;?>" onfocus="this.value = this.value">
</div>
<script>
     $(".searchItem").focus();
    $(function(){
        $(".searchItem").keyup(function(){
            clearTimeout(timer);
            timer = setTimeout(function(){
                var val = $("#search").val();
                openPage("search.php?target=" + val);
            }, 2000);
        });
    });
</script>

<?php 
if($target == "") exit(); 
?>
<div class="trackListContainer artistContainer">
    <h2 class="text-center">Results</h2>
    <ul>
        <?php 
        $songsQuery = mysqli_query($conn, "Select id from songs where title Like '$target%' Limit 10");
        if(mysqli_num_rows($songsQuery) == 0){
            echo "<h2 class='text-center'>Nothing find</h2>";
        }
            $songsArray = array();
        
            $i = 1;
            while($row=mysqli_fetch_array($songsQuery)){
                if($i>15){
                    break;
                }
               array_push($songsArray,$row['id']);
               $albumSong = new Song($conn,$row['id']);
               $albumArtist = $albumSong->getArtist();
               echo "<li class='trackListRow'>
                    <div class='trackCount text-center'>
                         <img class='playAlbum' src='./assets/images/icons/play-white.png' onclick='setTrack(\"" . $albumSong->getID() . "\", tempPlayList, true)'>
                         <span class='trackNumber'>$i.</span>
                    </div>
                    <div class='trackInfo'>
                        <span class='trackName'>" . $albumSong->getTitle() . "</span><br />
                        <span class='artistName'>" . $albumArtist->getName() . "</span>
                    </div>
                    <div class='trackOption'>
                    <img src='./assets/images/icons/more.png' alt='more options' class='optionButton' onclick='showOptionMenu(this)'>
                    </div>
                    <div class='trackDuration'>
                        <span class='artistName'>" . $albumSong->getDuration() . "</span>
                    </div>
               </li>";
               $i = $i +1;
            }
        ?>
        <script>
            var tempSongIds = '<?php echo json_encode($songsArray); ?>';
            tempPlayList = JSON.parse(tempSongIds);
        </script>
    </ul>
</div>

<div class="artistContainer">
<h2 class="text-center">Artist</h2>
<?php 
    $artistSQl = mysqli_query($conn, "Select id from artists where name Like '$target%' Limit 10");
    if(mysqli_num_rows($artistSQl) == 0){
        echo "<h2 class='text-center'>0 artists find</h2>";
    }
    while($row=mysqli_fetch_array($artistSQl)){
        $artistFound = new Artist($conn,$row['id']);
        echo "
            <div class='searchResultRow'>
                <div class='artstName'>
                    <span role='link' tabindex='0' onclick='openPage(\"artist.php?id=" . $artistFound->getID() . "\")'>
                        " .  $artistFound->getName() . "
                    </span>
                </div>
            </div>
        ";
    }
?>
</div>

<div class="gridViewContainer">
    <h2 class="text-center">Albums</h2>
            <?php
                $albumSQL = mysqli_query($conn, "Select * from albums where title like '$target%' Limit 10");
                if(mysqli_num_rows($albumSQL) == 0){
                    echo "<h2 class='text-center'>0 album find</h2>";
                }
                while($row = mysqli_fetch_array($albumSQL)){
                    echo 
                    "<div class='gridViewItem'>  
                        <a href='album.php?id=" . $row['id'] . "'>
                            <img src='" . $row['artworkPath'] . "'>
                                <div class='gridViewInfo text-center'>"
                                . $row['title'] .
                                "</div>
                        </a>
                    </div>";
                }
            ?>
</div>
<nav class="optionMenu">
            <input type="hidden" class="songID">
            <?php echo Playlist::getPlaylistDropdown($conn,$userLoggedIn->getUsername()); ?>
</nav>
