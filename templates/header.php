<?php 
include_once "./includes/dbConnect.php";
include_once './includes/classes/Artist.php';
include_once './includes/classes/Album.php';
include_once './includes/classes/Song.php';
if(isset($_SESSION['userLoggedIn'])){
    $userLoggedIn = $_SESSION['userLoggedIn'];
    echo "<script>userLoggedIn = ' $userLoggedIn'</script>";
  }
else{
    header("Location: register.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Spotifile">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">
  <meta name="author" content="RG">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Spotifle</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="./css/style.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
  <script src='./assets/js/script.js'> </script>

</head>
<body>
<div id="mainContainer">
    <div id="topContainer">
        <?php include 'nowPlayingBarContainer.php'; ?>
        <div id="mainViewContainer">
          <div id="mainContent">