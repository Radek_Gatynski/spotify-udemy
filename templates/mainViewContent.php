    <h1 class="text-center">You might also like</h1>
        <div class="gridViewContainer">
            <?php
                $albumSQL = mysqli_query($conn, "Select * from albums order by Rand() Limit 10");
                while($row = mysqli_fetch_array($albumSQL)){
                    echo 
                    "<div class='gridViewItem'>  
                        <a href='album.php?id=" . $row['id'] . "'>
                            <img src='" . $row['artworkPath'] . "'>
                                <div class='gridViewInfo text-center'>"
                                . $row['title'] .
                                "</div>
                        </a>
                    </div>";
                }
            ?>
        </div>