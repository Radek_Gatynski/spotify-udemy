<div id="navBarContainer">
            <nav class="navBar">
                <span class="logo" onclick="openPage('index.php')">
                    <img src="./assets/images/logo.PNG" alt="logo">
                    
                </span>
                <div class="group">
                    <div class="navItem searchItem">
                        <span onclick="openPage('search.php')" class="navItemLink">Search
                            <img src="./assets/images/icons/search.png" alt="search icon">
                        </span>    
                    </div>
                </div>
                <div class="group">
                    <div class="navItem">
                        <span onclick="openPage('browse.php')" class="navItemLink">Browse</span>    
                    </div>
                    <div class="navItem">
                        <span onclick="openPage('search.php')" class="navItemLink">Your music</span>    
                    </div>
                    <div class="navItem">
                        <span onclick="logout()" class="navItemLink">Settings</span>    
                    </div>
                    <button class="btn btn-info" onclick="openPage('logout.php')">Logout</button>
                </div>
            </nav>
</div>