<?php
$songSQL = mysqli_query($conn, "Select * from songs Order by albumOrder Limit  10");
$resultArray = array();

while($row = mysqli_fetch_array($songSQL)){
    array_push($resultArray, $row['id']);
}
$jsonArray = json_encode($resultArray);
?>

<script>
    $(document).ready(function(){
        var newPlayList = <?php echo $jsonArray; ?>;
        audioElement = new Audio();
        setTrack(newPlayList[0],newPlayList,false);    
        updateVolumeProgressBar(audioElement.audio);

        $("#nowPlayingBarContainer").on("mousedown touchstart mousemove touchmove", function(e){
            e.preventDefault();
        })


        $(".progressBar .progressBarBg").mousedown(function(){
            mouseDown = true;
        });
        
        $(".progressBar .progressBarBg").mousemove(function(e){
            if(mouseDown){
                timeFromOffset(e,this);
            }
        });

        $(".progressBar .progressBarBg").mouseup(function(e){
                timeFromOffset(e,this);
        });


        //volume
        $(".volumeProgressBar .progressVolume").mousedown(function(){
            mouseDown = true;
        });
        
        $(".volumeProgressBar .progressVolume").mousemove(function(e){
            if(mouseDown){
            var percentage = e.offsetX / $(this).width();
              if(percentage>=0 && percentage<=1){
                audioElement.audio.volume = percentage;
                if(percentage == 0){
                    var imageName = "volume.png";
                     $(".volume img").attr('src', './assets/images/icons/' + imageName);
                }else{
                    var imageName = "volume.png";
                     $(".volume img").attr('src', './assets/images/icons/' + imageName);
                }
            }
            }
        });

        $(".volumeProgressBar .progressVolume").mouseup(function(e){
            var percentage = e.offsetX / $(this).width();
              if(percentage>=0 && percentage<=1){
                audioElement.audio.volume = percentage;
                if(percentage == 0){
                    var imageName = "volume.png";
                     $(".volume img").attr('src', './assets/images/icons/' + imageName);
                }else{
                    var imageName = "volume.png";
                     $(".volume img").attr('src', './assets/images/icons/' + imageName);
                }
            }
        });

        $(document).mouseup(function(){
            mouseDown = false;
        })

    });

    function timeFromOffset(mouse, progressBarBg){
        var procent = mouse.offsetX / $(progressBarBg).width() * 100;
        var seconds = audioElement.audio.duration * (procent/100);
        audioElement.setTime(seconds);
    }

    function  nextSong() {
        if(repeat==true){
            audioElement.setTime(0);
            playSong();
            return;
        }
        if(currentIndex==currentPlayList.length-1){
            currentIndex = 0;
        }
        else{
            currentIndex++;
        }
        var trackToPlay = shuffle ? shufflePlayList[currentIndex] : currentPlayList[currentIndex];
        setTrack(trackToPlay,currentPlayList,true);
    }

    function previousSong() {
        if(currentIndex == 0 || audioElement.audio.currentTime >= 5){
            audioElement.setTime(0);
        }
        else{
            currentIndex--;
        }
        var trackToPlay = currentPlayList[currentIndex];
        setTrack(trackToPlay,currentPlayList,true);
    }

    function repeatSong(){
        repeat = !repeat;
        var imageName = repeat? "repeat-active.png" : "repeat.png";
        $(".repeat img").attr('src', './assets/images/icons/' + imageName);
    }

    function muteVolume(){
        audioElement.audio.muted = !audioElement.audio.muted;
        var imageName = audioElement.audio.muted? "volume-mute.png" : "volume.png";
        $(".volume img").attr('src', './assets/images/icons/' + imageName);
    }

    function setShuffle(){
        shuffle = !shuffle;
        var imageName = shuffle? "shuffle-active.png" : "shuffle.png";
        $(".shuffle img").attr('src', './assets/images/icons/' + imageName);
        //random or not-random playlist
        if(shuffle){
            shuffleArray(shufflePlayList);
            currentIndex = shufflePlayList.indexOf(audioElement.currentlePlaying.id);
        }else{
            currentIndex = currentPlayList.indexOf(audioElement.currentlePlaying.id);
        }
    }

    function shuffleArray(a) {
    var j, x, i;
        for (i = a.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = a[i];
            a[i] = a[j];
            a[j] = x;
        }
        return a;
    }


    function setTrack(trackID, newPlayList, play){
        if(newPlayList != currentPlayList){
            currentPlayList = newPlayList;
            shufflePlayList = currentPlayList.slice();
            shuffleArray(shufflePlayList);
        }

        if(shuffle){
            currentIndex = shufflePlayList.indexOf(trackID);

        }else{
            currentIndex = currentPlayList.indexOf(trackID);
        }  
        pauseSong();
        
        $.post("./includes/handles/ajax/getSongJson.php", { songID: trackID }, function(dataInfo) {     
            var track = JSON.parse(dataInfo);
            $(".trackInfo .albumTitle").text(track.title);

            $.post("./includes/handles/ajax/getArtistJson.php", { artistID: track.artist }, function(dataInfo) {    
                var artist = JSON.parse(dataInfo); 
                $(".trackInfo .artistTitle").text(artist.name); 
                $(".trackInfo .artistTitle").attr("onclick","openPage('artist.php?id=" + artist.id + "')");
            });

            $.post("./includes/handles/ajax/getAlbumJson.php", { albumID: track.album }, function(dataInfo) {    
                var album = JSON.parse(dataInfo); 
                $(".albumImage img").attr("src", album.artworkPath); 
                $(".albumImage img").attr("onclick","openPage('album.php?id=" + album.id + "')");            
                $(".trackInfo .albumTitle").attr("onclick","openPage('album.php?id=" + album.id + "')");
            });

             audioElement.setTrack(track);
           //  playSong();
             if(play){
                  playSong();
             }
        });

   
    }
    function playSong(){
        if(audioElement.audio.currentTime == 0){
           $.post("./includes/handles/ajax/updatePlays.php", {songID: audioElement.currentlePlaying.id});
        }  
            $('.play').hide();
            $('.pause').show();
            audioElement.play();
       
    }
    function pauseSong(){
            $('.pause').hide();
            $('.play').show();
            audioElement.pause();
    }  
</script>

<div id="nowPlayingBarContainer">
    <div id="containerPlay">
        <div class="leftColumnPlay">
            <div class="content">
                <div class="albumImage">
                    <img role="link" tabindex="0" class="albumBarImage" src="" alt="test">
                </div>
                <div class="trackInfo">
                    <span class="albumLink albumTitle" role="link" tabindex="0"></span><br />
                    <span class="albumLink artistTitle"  role="link" tabindex="0"></span>
                </div>
            </div>
        </div>
        <div class="centerColumnPlay">
            <div class="content playerControls">
                <div class="buttons">
                    <button class="controlButton shuffle" title="Shuffle button" onclick="setShuffle()">
                        <img src="./assets/images/icons/shuffle.png" alt="shuffle">
                    </button>

                    <button class="controlButton previous" title="Previous button" onclick="previousSong()">
                        <img src="./assets/images/icons/previous.png" alt="previous">
                    </button>

                    <button class="controlButton play" title="Play button" onclick="playSong()">
                        <img src="./assets/images/icons/play.png" alt="play">
                    </button>
                    <button class="controlButton pause" title="Pause button" onclick="pauseSong()" style="display:none;">
                        <img src="./assets/images/icons/pause.png" alt="pause">
                    </button>

                    <button class="controlButton next" title="Next button" onclick="nextSong()">
                        <img src="./assets/images/icons/next.png" alt="next">
                    </button>

                    <button class="controlButton repeat" title="Repeat button" onclick="repeatSong()">
                        <img src="./assets/images/icons/repeat.png" alt="repeat">
                    </button>
                </div>
                <div class="playbackBar">
                    <span class="progressTime current">0.00</span>
                    <div class="progressBar">
                        <div class="progressBarBg">
                            <div class="progressInfo"></div>
                        </div>
                    </div>
                    <span class="progressTime remaining">0.00</span>
                </div>
            </div>
        </div>
        <div class="rightColumnPlay">
            <div class="volumeBar">
                <button class="controlButton volume" title="Volume button" onclick="muteVolume()">
                    <img src="./assets/images/icons/volume.png" alt="volume icon">
                </button>
                <div class="volumeProgressBar">
                        <div class="progressVolume">
                            <div class="progressInfoVolume"></div>
                        </div>
                    </div>
            </div>
        </div>

    </div>
</div>