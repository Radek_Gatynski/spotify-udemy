<?php
include './includes/includedFiles.php';
?>
<div class="playlistContainer">
    <div class="gridViewContainer">
        <h2 class="text-center">Playlist</h2>
        <div class="buttonItem text-center">
            <button class="btn btn-success newPlaylist" onclick="createPlaylist()">New Playlist</button>
        </div>

        <div class="">
             <?php
                $username = $userLoggedIn->getUsername();
                $playlistSQL = mysqli_query($conn, "Select * from playlist where owner='$username'");
                if(mysqli_num_rows($playlistSQL) == 0){
                    echo "<h2 class='text-center'>0 playlist found</h2>";
                }
                while($row = mysqli_fetch_array($playlistSQL)){
                    $playlist = new Playlist($conn,$row);
                    echo 
                    "<div class='gridViewItem' role='link' tabindex='0' onclick='openPage(\"playlist.php?id=" . $playlist->getID() ."\")'>  
                        <div class='playlistImage'>
                            <img src='./assets/images/icons/playlist.png' alt='playlist'> 
                        </div>
                        <div class='gridViewInfo text-center'>"
                        . $row['name'] .
                        "</div>
                    </div>";
                }
            ?>
        </div>


    </div>
</div>